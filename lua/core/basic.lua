-- 创建局部变量
local opt = vim.opt -- 缩减vim接口长度

-- 行号
opt.relativenumber = true -- 开启相对行号
opt.number = true -- 开启行号

-- 缩进
opt.tabstop = 4 -- 设定制表符长度为4个空格
opt.shiftwidth = 4 -- 设定缩进长度为4个空格
opt.expandtab = true -- 默认展开制表符
opt.autoindent = true -- 自动缩进

-- 避免过长的代码无法在一个屏幕内显示
opt.wrap = false

-- 光标行显示
opt.cursorline = true

-- 启用鼠标
opt.mouse:append("a")

-- 启用系统粘贴板
opt.clipboard:append("unnamedplus")

-- 默认新窗口打开在右和下
opt.splitright = true
opt.splitbelow = true

-- 搜索
opt.ignorecase = true
opt.smartcase = true

-- 外观
opt.termguicolors = true
opt.signcolumn = "yes"

