-- 设置主键
vim.g.mapleader = " "

-- 声明局部变量keympa
local keymap = vim.keymap

-- -------- 插入模式 -------- --
keymap.set("i", "kk", "<ESC>")

-- neovide
if vim.g.neovide then
    keymap.set("i", "<S-Insert>", "<C-r>*")
end

-- -------- 可视模式 -------- --
-- 单行和多行移动
keymap.set("v", "J", ":m '>+1<CR>gv=gv")
keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- -------- 普通模式 -------- --
-- 取消高亮
keymap.set("n", "<C-h>n", ":nohl<CR>")


