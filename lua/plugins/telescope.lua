return {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.2',
    dependencies = { 'nvim-lua/plenary.nvim' },
	cmd = "Telescope",
	keys = {
		{ "<C-p>p", ":Telescope find_files<CR>", desc = "根据文件名查找(find_files)"},
		{ "<C-p>f", ":Telescope live_grep<CR>", desc = "根据文件内容查找(live_grep)"},	
		{ "<C-p>b", ":Telescope resume<CR>", desc = "返回上一个筛选器(resume)"},	
		{ "<C-p>h", ":Telescope oldfiles<CR>", desc = "最近编辑的文件(oldfiles)"},	
	},
	config = function ()
		
	end
}
