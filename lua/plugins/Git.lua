return {
    {
        "tpope/vim-fugitive",
        event = "VeryLazy",
        cmd = "Git"
    },
    {
        "lewis6991/gitsigns.nvim",
        event = "VeryLazy",
        opts = {
            signs = {
                add          = { text = '│' },
                change       = { text = '│' },
                delete       = { text = '_' },
                topdelete    = { text = '‾' },
                changedelete = { text = '~' },
                untracked    = { text = '┆' },
            },
            signcolumn = true,
            numhl = false,
            linehl = false,
            word_diff = false,
            watch_gitdir = {
                follow_files = true
            },
            attach_to_untracked = true,
            current_line_blame = false,
            current_line_blame_opts = {
                virt_text = true,
                virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
                delay = 1000,
                ignore_whitespace = false,
            },
            current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
            sign_priority = 6,
            update_debounce = 100,
            status_formatter = nil, -- Use defaults
            max_file_length = 40000, -- Disable if file is longer than this (in lines)
            preview_config = {
                border = 'single',
                style = 'minimal',
                relative = 'cursor',
                row = 0,
                col = 1
            },
            yadm = {
                enable = false
            }
        }
    },
    {
        "rhysd/conflict-marker.vim",
        event = "VeryLazy",
        config = function()
            vim.cmd([[
                let g:conflict_marker_highlight_group = ''

                let g:conflict_marker_begin = '^<<<<<<< .*$'
                let g:conflict_marker_end   = '^>>>>>>> .*$'

                highlight ConflictMarkerBegin guibg=#2f7366
                highlight ConflictMarkerOurs guibg=#2e5049
                highlight ConflictMarkerTheirs guibg=#344f69
                highlight ConflictMarkerEnd guibg=#2f628e
                highlight ConflictMarkerCommonAncestorsHunk guibg=#754a81
            ]])
        end
    }
}
