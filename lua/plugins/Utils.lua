return {
    {
        "kylechui/nvim-surround",
        event = "VeryLazy",
        version = "*",
        config = function()
            require("nvim-surround").setup({
c
            }) 
        end

    },
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {

        }
    },
    {
        "numToStr/Comment.nvim",
        event = "VeryLazy",
        config = function ()
            require("Comment").setup()
        end 
    },
}
