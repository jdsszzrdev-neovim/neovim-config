return {
	{
		"folke/tokyonight.nvim",
		lazy = true,
		priority = 1000,
		opts = {}
    },
    {
        "jinh0/eyeliner.nvim",
        event = "VeryLazy",
        opts = {
            highlight_on_key = true,
            dim = true
        }
    },
    {
        "nvim-lualine/lualine.nvim",
        opts = {
            options = {
                theme = "tokyonight",

            }
        },
        config = function ()
            require("lualine").setup{

            }
        end
    },
}
